from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.types import TypeDecorator, CHAR
from sqlalchemy.dialects.postgresql import UUID
import uuid
from datetime import date

def create_db(app):
    db = SQLAlchemy(app)

    class GUID(TypeDecorator):
        """Platform-independent GUID type.
        Uses PostgreSQL's UUID type, otherwise uses
        CHAR(32), storing as stringified hex values.
        """
        impl = CHAR

        def load_dialect_impl(self, dialect):
            if dialect.name == 'postgresql':
                return dialect.type_descriptor(UUID())
            else:
                return dialect.type_descriptor(CHAR(32))

        def process_bind_param(self, value, dialect):
            if value is None:
                return value
            elif dialect.name == 'postgresql':
                return str(value)
            else:
                if not isinstance(value, uuid.UUID):
                    return "%.32x" % uuid.UUID(value).int
                else:
                    # hexstring
                    return "%.32x" % value.int

        def process_result_value(self, value, dialect):
            if value is None:
                return value
            else:
                if not isinstance(value, uuid.UUID):
                    value = uuid.UUID(value)
                return value
            
    # Association table for the many-to-many relationship
    repas_recette_association = db.Table('repas_recette_association',
        db.Column('repas_id', db.Integer, db.ForeignKey('repas.id'), primary_key=True),
        db.Column('recette_id', db.Integer, db.ForeignKey('recette.id'), primary_key=True)
    )

    class Repas(db.Model):
        __tablename__ = 'repas'

        id = db.Column(db.Integer, primary_key=True)
        date_repas = db.Column(db.Date())
        moment = db.Column(db.String(100), nullable=False)
        nb_people = db.Column(db.Integer)
        recettes = db.relationship('Recette', secondary=repas_recette_association, backref='repas')
        user_id = db.Column(GUID(), db.ForeignKey('user.id'))
        group_id = db.Column('Group', db.ForeignKey('group.id'))

        def to_dict(repas):
            return {
                'id': repas.id,
                'date_repas': str(repas.date_repas),
                'day_of_week': str(repas.date_repas.weekday()),
                'moment': repas.moment,
                'number_people': repas.nb_people,
                'recettes': Recette.to_dict_given_array(repas.recettes)
            }

        def to_dict_given_array(repas_list):
            repas = []
            for rep in repas_list:
                dict = {
                    'id': rep.id,
                    'date_repas': str(rep.date_repas),
                    'day_of_week': str(rep.date_repas.weekday()),
                    'moment': rep.moment,
                    'number_people': rep.nb_people,
                    'recettes': Recette.to_dict_given_array(rep.recettes)
                }
                if (rep.date_repas >= date.today()):
                    repas.append(dict)

            return repas
        
        def to_dict_given_array_without_removing_dates(repas_list):
            repas = []
            for rep in repas_list:
                dict = {
                    'id': rep.id,
                    'date_repas': str(rep.date_repas),
                    'day_of_week': str(rep.date_repas.weekday()),
                    'moment': rep.moment,
                    'number_people': rep.nb_people,
                    'recettes': Recette.to_dict_given_array(rep.recettes)
                }
                repas.append(dict)

            return repas

        def __repr__(self):
            return f'Repas(id={self.id}, date_repas={self.date_repas}, moment={self.moment}, recette={self.recettes})'
        
    class Recette(db.Model):
        __tablename__ = 'recette'

        id = db.Column(db.Integer, primary_key=True)
        id_api = db.Column(db.Integer)
        title = db.Column(db.String(1000), nullable=False)
        image = db.Column(db.String(1000))
        nb_people = db.Column(db.Integer)
        ingredients = db.relationship('Ingredient', backref='recette')
        methodology = db.Column(db.Text)
        #repas_id = db.Column(db.Integer, db.ForeignKey('repas.id'))
        user_id = db.Column(GUID(), db.ForeignKey('user.id'))

        def to_dict(recette):
            return {
                'id': recette.id,
                'id_api': recette.id_api,
                'title': recette.title,
                'image': recette.image,
                'number_people': recette.nb_people,
                'ingredients': Ingredient.to_dict_given_array(recette.ingredients),
                'methodology': recette.methodology
            }
        
        def to_dict_given_array(recette_list):
            recettes = []
            for rec in recette_list:
                dict = {
                    'id': rec.id,
                    'id_api': rec.id_api,
                    'title': rec.title,
                    'image': rec.image,
                    'number_people': rec.nb_people,
                    'ingredients': Ingredient.to_dict_given_array(rec.ingredients),
                    'methodology': rec.methodology
                }
                recettes.append(dict)
            return recettes

        def __repr__(self):
            return f'Recette(id={self.id}, id_api={self.id_api}, title={self.title}, image={self.image}, ingredients={self.ingredients}, methodology={self.methodology})'
        
    class Ingredient(db.Model):
        __tablename__ = 'ingredient'
    
        id = db.Column(db.Integer, primary_key=True)
        quantity = db.Column(db.Integer)
        metric = db.Column(db.String(100))
        ingredient = db.Column(db.String(100))
        recette_id = db.Column(db.Integer, db.ForeignKey('recette.id'))

        def __repr__(self):
            return f'Ingredient(id={self.id}, quantity={self.quantity}, metric={self.metric}, ingredient={self.ingredient})'
        
        def to_dict_given_array(ingredient_list):
            ingredients = []
            for ingr in ingredient_list:
                dict = {
                    'id': ingr.id,
                    'quantity': ingr.quantity,
                    'metric': ingr.metric,
                    'ingredient': ingr.ingredient
                }
                ingredients.append(dict)
            return ingredients
        
    # Association table for the many-to-many relationship
    user_role_association = db.Table('user_role_association',
        db.Column('user_id', GUID(), db.ForeignKey('user.id'), primary_key=True),
        db.Column('group_id', db.Integer, db.ForeignKey('group.id'), primary_key=True)
    )


    class User(db.Model):
        __tablename__ = 'user'

        id = db.Column(GUID(), primary_key=True, default=lambda: str(uuid.uuid4()))
        mail = db.Column(db.String(100), nullable=False, unique=True)
        name = db.Column(db.String(100), nullable=False)
        encrypted_password = db.Column(db.LargeBinary(100), nullable=False)
        groups = db.relationship('Group', secondary=user_role_association, backref='users')
        repas = db.relationship('Repas', backref='user')
        recettes = db.relationship('Recette', backref='user')

        def get_password(self):
            return self.encrypted_password

        def __repr__(self):
            return f'User(id={self.id}, name={self.name}, mail={self.mail}, groups={self.groups}, repas={self.repas})'
        
        def to_dict(self):
            return {
                'id': str(self.id),
                'name': self.name,
                'mail': self.mail,
                'groups': Group.to_dict_given_array(self.groups),
                'repas': Repas.to_dict_given_array(self.repas),
                'recettes' : Recette.to_dict_given_array(self.recettes)
            }

    class Group(db.Model):
        __tablename__ = 'group'

        id = db.Column(db.Integer, primary_key=True)
        name = db.Column(db.String(500), nullable=False)
        isLocked = db.Column(db.Boolean, nullable=False, default=False)

        def to_dict(group):
            return {
                'id': group.id,
                'name': group.name,
                'islocked': group.isLocked
            }

        def to_dict_given_array(group_list):
            groups = []
            for gr in group_list:
                dict = {
                    'id': gr.id,
                    'name': gr.name,
                    'islocked': gr.isLocked
                }
                groups.append(dict)
            return groups
        
        def __repr__(self):
            return f'Group(id={self.id}, name={self.name}, islocked={self.isLocked})'
    


    with app.app_context():
        db.create_all()

    return db, User, Group, Recette, Repas, Ingredient