from flask import Flask, request
from database_config.models import *
import datetime
import bcrypt
import json
from flask_cors import CORS

from database_config import create_app


app = create_app()
db, User, Group, Recette, Repas, Ingredient = create_db(app)

cors = CORS(app, resources={r"/*": {"origins": "http://127.0.0.1:5173"}})

@app.route('/')
def hello():
    return 'Hello, World!'

@app.route('/create_user', methods=['POST'])
def create_user():
    # get basic informations
    user_mail = request.form.get('mail')
    user_name = request.form.get('name')
    password = request.form.get('password')

    encrypted_pass = bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt())

    user = User(mail=user_mail, name=user_name, encrypted_password=encrypted_pass)

    # record it
    db.session.add(user)
    db.session.commit()

    return json.dumps(user.to_dict())

@app.route('/login', methods=['GET'])
def login():
    user_mail = request.args.get('mail')
    password_to_check = request.args.get('password')

    print(user_mail)
    print(password_to_check)

    user = db.session.query(User).filter_by(mail=user_mail).first()

    if bcrypt.checkpw(password_to_check.encode('utf8'), user.encrypted_password ) :
        return json.dumps(user.to_dict())
    else :
        return "Le mot de passe n'est pas valable !"


@app.route('/get_info_user', methods=['GET'])
def get_info_user():

    user_id = request.args.get('user_id')

    user = db.session.query(User).get(user_id)

    return json.dumps(user.to_dict())


@app.route('/create_group', methods=['POST'])
def create_group():
    # we must add the group to the user who created it first
    user_id = request.form.get('user_id')
    user = db.session.query(User).get(user_id)
    
    # create group
    group_name = request.form.get('name')
    group = Group(name=group_name)

    # add it to the user list
    user.groups.append(group)

    #commit everything
    db.session.add(group)
    db.session.add(user)
    db.session.commit()

    return json.dumps(group.to_dict())

@app.route('/join_group', methods=['POST'])
def join_group():
    # get the user and group
    user_id = request.form.get('user_id')
    group_id = request.form.get('group_id')

    user = db.session.query(User).get(user_id)
    group = db.session.query(Group).get(group_id)

    if (group.isLocked is not True):
        # add it to the user list
        user.groups.append(group)

        # commit everything
        db.session.add(user)
        db.session.commit()

        return json.dumps(user.to_dict())
    
    return 'False'

@app.route('/quit_group', methods=['POST'])
def quit_group():
    # get the user and group
    user_id = request.form.get('user_id')
    group_id = request.form.get('group_id')

    user = db.session.query(User).get(user_id)
    group = db.session.query(Group).get(group_id)

    # add it to the user list
    user.groups.remove(group)

    # commit everything
    db.session.add(user)
    db.session.commit()

    return str(True)

@app.route('/lock_group', methods=['POST'])
def lock_group():
    # get the group
    group_id = request.form.get('group_id')
    group = db.session.query(Group).get(group_id)

    # change lock to true so nobody can enter it anymore
    group.isLocked = True

    # commit everything
    db.session.add(group)
    db.session.commit()

    return str(True)

@app.route('/unlock_group', methods=['POST'])
def unlock_group():
    # get the group
    group_id = request.form.get('group_id')

    group = db.session.query(Group).get(group_id)

    # change lock to true so anybody can enter it
    group.isLocked = False

    # commit everything
    db.session.add(group)
    db.session.commit()

    return str(True)

@app.route('/create_recette', methods=['POST'])
def create_recette():
    # we want to directly register the recette in the user list so he can find it quickly
    user_id = request.form.get('user_id')
    user = db.session.query(User).get(user_id)

    # create recette
    recette_title = request.form.get('title')
    recette_api_id = request.form.get('id_api')
    recette_ingredients = request.form.get('ingredients')
    recette_methodo = request.form.get('methodology')
    nb_people = request.form.get('nb_people')

    ingredients = json.loads(recette_ingredients)
    
    recette = Recette(title=recette_title, id_api=recette_api_id, nb_people=nb_people, methodology=recette_methodo)


    # add recette to list of recette of user
    user.recettes.append(recette)

    for ingr in ingredients:
        ingredient = Ingredient(
            quantity=ingr['quantity'],
            metric=ingr['metric'],
            ingredient=ingr['ingredient']
        )
        recette.ingredients.append(ingredient)

    #commit everything
    db.session.add(recette)
    db.session.add(user)
    db.session.commit()

    return json.dumps(recette.to_dict())

@app.route('/get_info_recette', methods=['GET'])
def get_info_recette():
    # create recette
    recette_id = request.args.get('recipe_id')

    recette = db.session.query(Recette).get(recette_id)

    return json.dumps(recette.to_dict())

@app.route('/record_recette', methods=['POST'])
def record_recette_to_user_list():
    # create recette
    recette_id = request.form.get('recette_id')
    user_id = request.form.get('user_id')

    recette = db.session.query(Recette).get(recette_id)
    user = db.session.query(User).get(user_id)

    # add recette to list of recette of user
    user.recettes.append(recette)

    return json.dumps(recette.to_dict())

@app.route('/find_by_api_id', methods=['GET'])
def find_by_api_id():
    id_api = request.args.get('api_id')

    recette_api = db.session.query(Recette).filter_by(id_api=id_api).first()

    if (recette_api is None):
        return str(None)
    else:
        return str(recette_api.id)

@app.route('/create_repas', methods=['POST'])
def create_repas():
    # get infos
    recette_id = request.form.get('recipe_id')
    date_repas = request.form.get('date_repas')
    moment = request.form.get('moment')
    user_id = request.form.get('user_id')
    group_id = request.form.get('group_id')
    nb_people = request.form.get('nb_pers')


    # get user and recette
    user = db.session.query(User).get(user_id)
    recette = db.session.query(Recette).get(recette_id)

    # create repas
    d, m, y = date_repas.split('-')
    repas = Repas(date_repas=datetime.date(int(y), int(m), int(d)), moment=moment, nb_people=nb_people, user_id=user_id, group_id=group_id)

    # add it to the user list
    user.repas.append(repas)
    repas.recettes.append(recette)

    #commit everything
    db.session.add(user)
    db.session.add(repas)
    db.session.add(recette)
    db.session.commit()

    return json.dumps(Repas.to_dict(repas))

@app.route('/get_info_repas', methods=['GET'])
def get_info_repas():
    repas_id = request.args.get('repas_id')

    repas = db.session.query(Repas).get(repas_id)

    return json.dumps(Repas.to_dict(repas))


@app.route('/delete_repas', methods=['POST'])
def delete_repas():
    # create recette
    repas_id = request.form.get('repas_id')

    repas = db.session.query(Repas).get(repas_id)

    db.session.delete(repas)
    db.session.commit()

    return 'Repas supprimé.'


@app.route('/get_repas_by_date', methods=['GET'])
def get_repas_by_date():
    # get group
    group_id = request.args.get('group_id')
    # get date
    date_repas = request.args.get('date_repas')

    d, m = date_repas.split('.')
    date = datetime.date(datetime.date.today().year, int(m), int(d))

    repas = db.session.query(Repas).\
        filter_by(date_repas=date).\
        filter_by(group_id=group_id).all()

    return json.dumps(Repas.to_dict_given_array_without_removing_dates(repas))


@app.route('/get_repas_by_range_date', methods=['GET'])
def get_repas_by_range_date():
    # get group
    group_id = request.args.get('group_id')
    # get date
    from_date_repas = request.args.get('from_date_repas')
    to_date_repas = request.args.get('to_date_repas')

    # change form of date objects
    d1, m1, y1 = from_date_repas.split('-')
    d2, m2, y2 = to_date_repas.split('-')

    from_date = datetime.date(int(y1), int(m1), int(d1))
    to_date = datetime.date(int(y2), int(m2), int(d2))

    # query to get the repas by date range
    repas = db.session.query(Repas).\
        filter(Repas.date_repas>=from_date).\
        filter(Repas.date_repas<=to_date).\
        filter_by(group_id=group_id).all()

    return json.dumps(Repas.to_dict_given_array_without_removing_dates(repas))



if __name__ == '__main__':
    app.run(debug=True)